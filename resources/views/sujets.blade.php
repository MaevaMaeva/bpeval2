<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"><link rel="stylesheet" href="./css/app.css">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Liste des Sujets-Le carnet des artistes</title>
   
        @include('header')
        
        <main>
            <h2>Proposer un nouveau sujet :</h2>
            <form id="form" class="text-center p-4" action="{{route('creationSujet')}}" method='get'>
                <label class="m-2" for="">Votre sujet d'impro :</label>
                <textarea name="newSujet" class="form-control" type="text"></textarea>
                <button  class="btn btn-lg btn-block btn-secondary text-white m-3" type="submit">Soumettre</button>
            </form>
            <h3>Liste des sujets disponibles :</h3>
            <ul>
                @foreach ($liste as $sujet)
                    @if ($sujet->dispo)
                        <li>{{$sujet->text}}</li>
                    @endif
                @endforeach
            </ul>
        </main>
        @include('footer')
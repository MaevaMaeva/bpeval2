<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"><link rel="stylesheet" href="./css/app.css">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Roulette-Le carnet des artistes</title>
   
    @include('header')
        
        <main class="container">
            <div class="form card" >
                <div class="card-body">
                    <form action="{{route('afficheSujet')}}">
                    <a href="/roulette" class="btn btn-lg btn-block btn-secondary">Nouveau Sujet</a>
                    <input type="hidden" name="idSujet" value="{{$sujets->id}}">
                    <button  type="submit" class="btn btn-lg btn-block btn-secondary">Jouer ce Sujet</button>
                    </form>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{$sujets->text}}</h5>
                </div>
                <img class="card-img-right" src="https://media1.giphy.com/media/26uflBhaGt5lQsaCA/giphy.gif" alt="gif roulette">
            </div>
        </main>
        @include('footer')
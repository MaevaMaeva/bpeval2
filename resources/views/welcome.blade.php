<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"><link rel="stylesheet" href="./css/app.css">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Accueil-Le carnet des artistes</title>
   
    @include('header')
        
        <main>
            <h3>Bienvenue dans le carnet des Artistes !</h3>
            <p>La "société des beaux-parleurs" est un club d'improvisation théatrale, 
            dont les membres se réunissent chaque jeudi soir pour s'adonner à leur pratique préférée. 
            L'impro théatrale se déroule sous forme de "battles" : </p>
            <p>l'arbitre au centre, et deux équipes de 2 ou 3 personnes de chaque côté. 
            L'arbitre annonce un sujet et les deux teams commencent alors à improviser.</p>
            <p>Parfois, l'arbitre mentionne une contrainte supplémentaire :</p>
            <p>l'impro se déroule en language <a href="#gromlo">"gromlo"</a>.</p>
            <p>À la fin du battle, une équipe est déclarée gagnante par l'arbitre, 
            qui rend son verdict à l'applaudimètre : </p>
            <p>en invitant le public à faire un maximum de bruit pour l'équipe qui l'a le plus convaincu !</p>
            <img src="https://dirlida.fr/wp-content/uploads/2017/07/improvisation-toulouse-noir-blanc.jpg" alt="photo d'impro">
            <h3 id='gromlo'>Le Gromlo, c'est quoi ?</h3>
            <p>Le gromlo est une technique de théâtre et d’improvisation extrêmement pratique.
            </p>
            <p>Face à de jeunes acteurs impressionnés,  craignant de ne pas savoir quoi dire… quoi sortir de leur bouche…
            </p>
            <p>Le gromlo règle la question : il ne racontera rien, pas besoin d’idées,  pas besoin de connaître des mots.
            </p>
            <p>Il s’ agit de parler en “yaourt”, de dire ce qui vous passe par la tête.  Gra mameu besigoto babababanoinoi… etc etc…
            </p>
            <p>L’intérêt : affronter toutes les situations de jeu sans être jugés sur nos idées.</p>
            <p>Commencer à jouer sans commencer par construire des murs.</p>
            <p>Avec le gromlo, on va pouvoir traverser les intentions, les émotions, le jeu et l’échange, sans avoir à penser à autre chose.
            </p>

        </main>
        @include('footer')
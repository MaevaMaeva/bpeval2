<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class SujetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 20; $i++) {
            $pagesFamille = Http::get('http://www.omdbapi.com/?s=famille&page='.$i.'&apikey=c09289cb');
            foreach($pagesFamille['Search'] as $film){
                $response= Http::get('http://www.omdbapi.com/?i='.$film['imdbID'].'&plot=short&apikey=c09289cb&');

                // Debug
                $this->command->info("Import du film " . $response['Title']);
                if ($response['Plot']==="N/A"){
                    DB::table('sujets')->insert([
                        'text' => $response['Title']
                    ]);
                } else{
                    DB::table('sujets')->insert([
                        'text' => $response['Plot']
                    ]);
                }
        
            }
        }
    }
}
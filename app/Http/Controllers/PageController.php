<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sujet;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    
    function afficheAccueil(){
        return view('welcome');
    }
    function afficheRoulette(){
        return view('roulette',['sujets' => Sujet::where('dispo', 1 )->inRandomOrder()->first()]);
    }
    function afficheDates(){
        return view('dates');
    }
    function afficheSujets(){
        return view('sujets',['liste' => Sujet::all()]);
    }
    public function creationSujet(Request $request)
    {
        if ($request->input("newSujet") != null) {
            $proposition = $request->input("newSujet");
            $insertionNouveau = new Sujet;
            $insertionNouveau->text = $proposition;
            $insertionNouveau->save();
            return view('sujets', ['liste' => Sujet::all()]);
        }
        return view('sujets',['liste' => Sujet::all()]);
    }
    function afficheSujet(Request $request){
        $idRecup = $request->input("idSujet");
        DB::table('sujets')
              ->where('id', $idRecup)
              ->update(['dispo' => 0]);
            // $insertionNouveau = new Sujet;
            // $insertionNouveau->text = $proposition;
            // $insertionNouveau->save();
            return view('sujet', ['sujet' => Sujet::where('id', $idRecup)->first()]);
    }
    
}

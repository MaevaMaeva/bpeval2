<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'afficheAccueil']);
Route::get('/roulette', [PageController::class, 'afficheRoulette']);
Route::get('/dates', [PageController::class, 'afficheDates']);
Route::get('/sujets', [PageController::class, 'afficheSujets']);
Route::get('/sujet', [PageController::class, 'afficheSujet'])->name('afficheSujet');
Route::get('/creationSujet', [PageController::class, 'creationSujet'])->name('creationSujet');